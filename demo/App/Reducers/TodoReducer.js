import {ADD_TASK, MARK_AS_COMPLETE, DELETE_TASK} from '../Actions/Keys';
import DefaultState from './Default';

export const INITIAL_STATE = DefaultState.todo;

export const TodoReducer = (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case ADD_TASK: {
      let {pending, completed} = state;
      pending = [...state.pending, action.payload];
      return {pending, completed};
    }
    case MARK_AS_COMPLETE: {
      let {pending, completed} = state;
      pending = [
        ...state.pending.slice(0, action.payload),
        ...state.pending.slice(action.payload + 1),
      ];
      completed = [
        ...state.completed,
        ...state.pending.slice(action.payload, action.payload + 1),
      ];
      return {pending, completed};
    }
    case DELETE_TASK: {
      let {pending, completed} = state;
      if (action.from === 'pending') {
        pending = [
          ...state.pending.slice(0, action.payload),
          ...state.pending.slice(action.payload + 1),
        ];
      } else {
        completed = [
          ...state.completed.slice(0, action.payload),
          ...state.completed.slice(action.payload + 1),
        ];
      }
      return {pending, completed};
    }
    default:
      return state;
  }
};

export default TodoReducer;
