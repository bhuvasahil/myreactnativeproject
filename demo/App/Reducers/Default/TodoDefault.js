const todoDefault = {
  pending: [
    {
      id: 1,
      name: 'Task1',
      due: Date.now(),
    },
  ],
  completed: [
    {
      id: 2,
      name: 'Task2',
      due: Date.now(),
    },
  ],
};

export default todoDefault;
