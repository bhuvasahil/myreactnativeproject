import UserDefault from './UserDefault';
import TodoDefault from './TodoDefault';

const appDefaultReducer = {
  user: UserDefault,
  todo: TodoDefault,
};

export default appDefaultReducer;
