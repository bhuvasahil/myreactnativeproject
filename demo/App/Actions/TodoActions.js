import {ADD_TASK, MARK_AS_COMPLETE, DELETE_TASK} from './Keys';

export const addTask = todo => {
  return {
    type: ADD_TASK,
    payload: todo,
  };
};

export const markAsComplete = index => {
  return {
    type: MARK_AS_COMPLETE,
    payload: index,
  };
};

export const deleteTask = (from, index) => {
  return {
    type: DELETE_TASK,
    from: from,
    payload: index,
  };
};
