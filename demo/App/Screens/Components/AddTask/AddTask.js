import React, {useContext, useState} from 'react';
import {Button, Platform, SafeAreaView, TextInput, View} from 'react-native';
import CommonStyle from '../../../Theme/CommonStyle';
import {AppContext} from '../../../AppContext';
import {useDispatch} from 'react-redux';
import {addTask} from '../../../Actions/TodoActions';
import {ButtonComponent} from '../../SubComponents';
import DateTimePicker from '@react-native-community/datetimepicker';
import {ScheduleNotification} from '../../../Services/NotificationService';
import PushNotification from 'react-native-push-notification';

const AddTask = props => {
  const {appTheme} = useContext(AppContext);
  const [name, setName] = useState('');
  const [due, setDue] = useState(new Date(Date.now()));
  const [mode, setMode] = useState('date');
  const [show, setShow] = useState(false);
  const dispatch = useDispatch();

  const add = id => {
    dispatch(
      addTask({
        name: name,
        due: new Date(Date.now() + 10000),
        id: id,
      }),
    );
    props.navigation.navigate('Home');
  };

  const onChange = (event, selectedDate) => {
    const currentDate = selectedDate || due;
    setShow(Platform.OS === 'ios');
    setDue(currentDate);
  };

  const showMode = currentMode => {
    setShow(true);
    setMode(currentMode);
  };

  const showDatepicker = () => {
    showMode('date');
  };

  const showTimepicker = () => {
    showMode('time');
  };

  return (
    <SafeAreaView
      style={[
        CommonStyle.flexContainer,
        {backgroundColor: appTheme.background},
      ]}>
      <TextInput
        style={{color: appTheme.text}}
        placeholder="Enter new Todo"
        onChangeText={TextInputValueHolder => setName(TextInputValueHolder)}
      />
      <View>
        <Button onPress={showDatepicker} title="Pick date" />
      </View>
      <View>
        <Button onPress={showTimepicker} title="Pick time" />
      </View>
      {show && (
        <DateTimePicker
          value={due}
          mode={mode}
          is24Hour={true}
          display="default"
          onChange={onChange}
        />
      )}
      <ButtonComponent
        title={'Add Task'}
        style={{borderRadius: 40}}
        onPress={() => {
          const id = Date.now();
          add(id);
        }}
      />
    </SafeAreaView>
  );
};

export default AddTask;
