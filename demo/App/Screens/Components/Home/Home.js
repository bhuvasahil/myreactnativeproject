import React, {useContext} from 'react';
import {Alert, SafeAreaView, TouchableOpacity, View} from 'react-native';
import {CustomText} from '../../CommonComponent';
import CommonStyle from '../../../Theme/CommonStyle';
import {AppContext} from '../../../AppContext';
import {shallowEqual, useDispatch, useSelector} from 'react-redux';
import {deleteTask, markAsComplete} from '../../../Actions/TodoActions';
import PushNotification from 'react-native-push-notification';
const channelId = '123';
const channelName = 'My Channel';

const Home = props => {
  const {appTheme} = useContext(AppContext);
  const todos = useSelector(state => state.todo, shallowEqual);
  const dispatch = useDispatch();

  PushNotification.removeAllDeliveredNotifications();
  for (let i = 0; i < todos.pending.length; i++) {
    PushNotification.localNotificationSchedule({
      channelId: channelId,
      title: 'Reminder',
      id: todos.pending[i].id,
      message: todos.pending[i].name + ' is pending.', // (required)
      date: new Date(todos.pending[i].due), // in 60 secs
      allowWhileIdle: true, // (optional) set notification to work while on doze, default: false
    });
  }
  PushNotification.getScheduledLocalNotifications(callback => {
    console.log(callback);
  });

  const openCompletedPopup = key =>
    Alert.alert('Delete todo', 'Do you want to delete this task?', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'OK',
        onPress: () => {
          dispatch(deleteTask('completed', key));
        },
      },
    ]);

  const openPendingPopup = key =>
    Alert.alert('Edit todo', 'Press button to continue', [
      {
        text: 'Cancel',
        onPress: () => console.log('Cancel Pressed'),
        style: 'cancel',
      },
      {
        text: 'Mark as completed',
        onPress: () => {
          dispatch(markAsComplete(key));
        },
      },
      {
        text: 'Delete',
        onPress: () => {
          dispatch(deleteTask('pending', key));
        },
      },
    ]);

  // const goToNextScreen = page => {
  //   const {navigation} = this.props;
  //   navigation.dispatch(
  //     CommonActions.reset({
  //       index: 0,
  //       routes: [{name: page}],
  //     }),
  //   );
  // };

  // axios
  //   .get(ApiConfig.secure)
  //   .then(async token => {
  //     setTodos(token.data.todos[0]);
  //   })
  //   .catch(async error => {
  //     Alert.alert('Something went wrong', 'Please login again to continue');
  //     await removeStoreItem('token');
  //     goToNextScreen('Login');
  //   });

  return (
    <SafeAreaView
      style={[
        CommonStyle.flexContainer,
        {backgroundColor: appTheme.background},
      ]}>
      {/*<CustomText xlarge style={{color: appTheme.text}}>*/}
      {/*  {todos}*/}
      {/*</CustomText>*/}
      <CustomText
        xlarge
        style={{color: appTheme.text, backgroundColor: '#39821b', padding: 12}}>
        Pending TODOs
      </CustomText>
      {todos.pending.map((prop, key) => {
        const date = new Date(prop.due);
        return (
          <View key={key}>
            <TouchableOpacity onPress={() => openPendingPopup(key)}>
              <CustomText xlarge style={{color: appTheme.text, margin: 12}}>
                {prop.name}
                {'\n'}
                Due: {date.toLocaleDateString()} - {date.toLocaleTimeString()}
              </CustomText>
            </TouchableOpacity>
          </View>
        );
      })}
      <CustomText
        xlarge
        style={{color: appTheme.text, backgroundColor: '#39821b', padding: 12}}>
        Completed TODOs
      </CustomText>
      {todos.completed.map((prop, key) => {
        const date = new Date(prop.due);
        return (
          <View key={key}>
            <TouchableOpacity onPress={() => openCompletedPopup(key)}>
              <CustomText xlarge style={{color: appTheme.text, margin: 12}}>
                {prop.name}
                {'\n'}
                Due: {date.toLocaleDateString()} - {date.toLocaleTimeString()}
              </CustomText>
            </TouchableOpacity>
          </View>
        );
      })}
    </SafeAreaView>
  );
};

export default Home;
