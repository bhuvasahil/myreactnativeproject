import Config from 'react-native-config';

const productionUrl = 'http://myapi.com/';

const developmentUrl = 'http://192.168.0.198:8080';

const ENVIRONMENT = {
  PROD: 'PROD',
  DEV: 'DEV',
};

const currentEnv = ENVIRONMENT.DEV;

const baseUrl =
  (currentEnv === ENVIRONMENT.PROD && productionUrl) || developmentUrl;

const baseUrlApi = `${baseUrl}/`;

let ApiConfig = {
  baseUrl,
  baseUrlApi,
  token: null,
  login: `${baseUrlApi}user/login`,
  secure: `${baseUrlApi}user/secure`,
};

export {ApiConfig};
