const AppImages = {
  passwordOpen: require('./Images/password_open.png'),
  passwordClosed: require('./Images/password_closed.png'),
  home: require('./Images/home.png'),
  search: require('./Images/search.png'),
  settings: require('./Images/settings.png'),
  user: require('./Images/profile.png'),
  tick: require('./Images/tick.png'),
  addTask: require('./Images/add_task.png'),
};

export default AppImages;
