import PushNotification, {Importance} from 'react-native-push-notification';
import {Platform} from 'react-native';

const channelId = '123';
const channelName = 'My Channel';

export const ScheduleNotification = (message, date, id) => {
  PushNotification.localNotificationSchedule({
    channelId: channelId,
    id: id,
    message: message, // (required)
    date: date, // in 60 secs
    allowWhileIdle: true, // (optional) set notification to work while on doze, default: false
  });
  return {
    type: '',
  };
};

export const DeleteNotification = id => {
  PushNotification.cancelLocalNotifications({id: id});
  return {
    type: '',
  };
};