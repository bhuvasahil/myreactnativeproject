var http = require('http');
var port = 8080;
var express = require('express');
var app = express();
var cors = require('cors');
var bodyParser = require('body-parser');
const passport = require('passport')
var Strategy = require('passport-http-bearer').Strategy;
const jwt = require("jsonwebtoken");
var morgan = require('morgan')

app.use(morgan('combined'))

passport.use(new Strategy(
    function(token, done){
      try {
        console.log(token);
        const decoded = jwt.verify(token, "JWTKEY");
        console.log(decoded);
        return done(null, decoded);
      }
      catch(err){
          console.log(err);
        return done(null, false);
      }
    }
))

app.use(passport.initialize());
app.use(passport.session());
app.use(bodyParser.json());
app.use(cors());
const userRoute = require('./routes/user-routes');
app.use('/user', userRoute);

app.use((req, res, next) => {
    const error = new Error("Not found");
    error.status = 404;
    next(error);
});

app.use((error, req, res, next) => {
    res.status(error.status || 500);
    res.json({
        error: {
            message: error.message
        }
    });
});

http.createServer(app).listen(port);


console.log('Hello from Backend port :', port);