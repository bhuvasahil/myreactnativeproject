const express = require("express");
const userRoute = express.Router();

const passport = require('passport');
const jwt = require("jsonwebtoken");


const secret = "JWTKEY"

//POST: localhost:8080/user/login
userRoute.post("/login", (req, res) => {
    console.log("localhost/user/login");
    console.log(req.body.email, req.body.password);
    if (req.body.email == 'admin' && req.body.password == 'admin') {
        const token = jwt.sign(
            {
                email: req.body.email,
            },
            secret,
            {
                expiresIn: "1h"
            }
        );
        return res.status(200).json({
            message: "Auth successful",
            token: token
        });
    } else {
        return res.status(401).json({
            message: "Login failed"
        });
    }
});

userRoute.get("/secure", passport.authenticate('bearer', { session: false }), (req, res) => {
    return res.status(200).json({
        todos: ["Complete Tasks"],
    });
})

module.exports = userRoute;